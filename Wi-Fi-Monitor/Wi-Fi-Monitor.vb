﻿Imports System.Timers
Imports System.Diagnostics


Public Class Service1

    Private timer As Timer
    Private eventLogger As New EventLog

    Protected Overrides Sub OnStart(ByVal args() As String)
        ComenceTimer()
    End Sub

    Protected Overrides Sub OnContinue()
        MyBase.OnContinue()
        ComenceTimer()
    End Sub

    Protected Overrides Function OnPowerEvent(powerStatus As ServiceProcess.PowerBroadcastStatus) As Boolean
        Select Case (powerStatus)
            Case ServiceProcess.PowerBroadcastStatus.Suspend
                timer.Stop()
            Case ServiceProcess.PowerBroadcastStatus.ResumeSuspend
                ComenceTimer()
        End Select
        Return MyBase.OnPowerEvent(powerStatus)
    End Function

    Protected Overrides Sub OnPause()
        MyBase.OnPause()
        timer.Stop()
    End Sub

    Protected Overrides Sub OnStop()
        timer.Stop()
    End Sub

    Private Sub ComenceTimer()
        Me.timer = New Timer(10000)
        timer.AutoReset = True
        timer.Enabled = True
        AddHandler timer.Elapsed, New ElapsedEventHandler(AddressOf ScanNetwork)
        timer.Start()
    End Sub

    Private Sub ScanNetwork(sender As Object, e As ElapsedEventArgs)
        Dim cmd As New Process()
        cmd.StartInfo.CreateNoWindow = True
        cmd.StartInfo.FileName = "netsh"
        cmd.StartInfo.Arguments = "wlan show networks"
        cmd.StartInfo.RedirectStandardOutput = True
        cmd.StartInfo.UseShellExecute = False
        cmd.Start()
        Dim output As String = cmd.StandardOutput.ReadToEnd()
        ' RUSTIQUE
        If output.IndexOf("There are 0") <> -1 Then 'Only to be triggered by the specific problem we found
            RestartAdapter()
        End If
    End Sub

    Private Sub RestartAdapter()
        Dim stopProcess As New Process()
        stopProcess.StartInfo.CreateNoWindow = True
        stopProcess.StartInfo.FileName = "netsh"
        stopProcess.StartInfo.Arguments = "int set interface name=" & ControlChars.Quote & "Wi-Fi" _
            & ControlChars.Quote & " DISABLED"
        stopProcess.StartInfo.RedirectStandardOutput = True
        stopProcess.StartInfo.UseShellExecute = False
        stopProcess.Start()
        stopProcess.WaitForExit()
        Dim startProcess As New Process()
        startProcess.StartInfo.CreateNoWindow = True
        startProcess.StartInfo.FileName = "netsh"
        startProcess.StartInfo.Arguments = "int set interface name=" & ControlChars.Quote & "Wi-Fi" _
            & ControlChars.Quote & " ENABLED"
        startProcess.StartInfo.RedirectStandardOutput = True
        startProcess.StartInfo.UseShellExecute = False
        startProcess.Start()
        startProcess.WaitForExit()
    End Sub

End Class
